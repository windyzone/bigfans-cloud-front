/**
 * Created by lichong on 2/22/2018.
 */

import React from 'react';
import {Carousel , Button , Tabs } from 'antd';
import HttpUtils from 'utils/HttpUtils'

class ImageGallery extends React.Component {
    state = {
        imageGroups: [
            {
                largeImg: 'https://img11.360buyimg.com/n1/jfs/t3415/158/45736502/99512/8c924430/57fee529Nf9d02724.jpg',
                smallImg: 'https://img11.360buyimg.com/n5/jfs/t4894/201/1592462998/178587/b5f85202/58f1f946N9f809f8c.jpg'
            },
            {
                largeImg: 'https://img11.360buyimg.com/n1/jfs/t3385/284/87919038/36274/f64b30b0/57fee527Nd5870cc3.jpg',
                smallImg: 'https://img11.360buyimg.com/n5/jfs/t3385/284/87919038/36274/f64b30b0/57fee527Nd5870cc3.jpg'
            }
        ]
    };

    componentDidMount() {
        let self = this;
        let prodId = this.props.prodId;
        HttpUtils.getProductImages({params: {prodId}}, {
            success(resp){
                self.setState({imageGroups : resp.data})
            },
            error (resp){

            }
        })
    }

    changeSlide(index){
        this.refs.imageContainer.goTo(index);
    }

    render() {
        return (
            <div>
                <Carousel effect="fade" ref="imageContainer" dots={false}>
                    {
                        this.state.imageGroups.map((imgGroup, index) => {
                            return (
                                <div key={index}><img style={{width:350 , height:350}} src={imgGroup.server + imgGroup.largeImg}/></div>
                            )
                        })
                    }
                </Carousel>
                <Tabs
                    defaultActiveKey="0"
                    style={{ height: 80 }}
                    onChange={(index) => this.changeSlide(index)}
                >
                    {
                        this.state.imageGroups.map((imgGroup, index) => {
                            return (
                                <Tabs.TabPane tab={<img style={{width:54 , height:54}} src={imgGroup.server + imgGroup.smallImg}></img>} key={index}></Tabs.TabPane>
                            )
                        })
                    }
                </Tabs>
            </div>
        );
    }
}
export default ImageGallery;